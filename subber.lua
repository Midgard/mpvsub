--------------
-- Subtitle creation utility for mpv
-- @author midgard
-- @author ctk
-- @license GPL-3.0-or-later

-- subber: create subtitles within mpv
-- Copyright (C) 2025, midgard and ctk
--
-- This program is free software: you can redistribute it and/or modify it under the terms of
-- the GNU General Public License as published by the Free Software Foundation, either version 3
-- of the License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
-- without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
-- See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with this program.
-- If not, see <https://www.gnu.org/licenses/>.



-- Variable name convention: the prefix mb_ (read: "maybe") indicates that a variable may be nil

-- Functions to assert the type of variables, passing through their arguments
-- luacheck: push ignore 631 211
local function a_table       (...) for _, x in ipairs({...}) do assert(            type(x) == "table"   ) end; return ... end
local function a_number      (...) for _, x in ipairs({...}) do assert(            type(x) == "number"  ) end; return ... end
local function a_string      (...) for _, x in ipairs({...}) do assert(            type(x) == "string"  ) end; return ... end
local function a_boolean     (...) for _, x in ipairs({...}) do assert(            type(x) == "boolean" ) end; return ... end
local function a_function    (...) for _, x in ipairs({...}) do assert(            type(x) == "function") end; return ... end
local function a_userdata    (...) for _, x in ipairs({...}) do assert(            type(x) == "userdata") end; return ... end
local function a_mb_table    (...) for _, x in ipairs({...}) do assert(x == nil or type(x) == "table"   ) end; return ... end
local function a_mb_number   (...) for _, x in ipairs({...}) do assert(x == nil or type(x) == "number"  ) end; return ... end
local function a_mb_string   (...) for _, x in ipairs({...}) do assert(x == nil or type(x) == "string"  ) end; return ... end
local function a_mb_boolean  (...) for _, x in ipairs({...}) do assert(x == nil or type(x) == "boolean" ) end; return ... end
local function a_mb_function (...) for _, x in ipairs({...}) do assert(x == nil or type(x) == "function") end; return ... end
local function a_mb_userdata (...) for _, x in ipairs({...}) do assert(x == nil or type(x) == "userdata") end; return ... end
-- luacheck: pop
-- luacheck: globals mp

-- Preferably do not modify these options in this file but in script-opts/subber.conf
-- in your mpv user folder like so:
-- cps-warn=10
-- offset=0.1
--
-- You can also override these options on the command line like so:
--   --script-opts=subber-cps-warn=10,subber-offset=0.1
local options = {
    -- When to start displaying the characters per second in red
    ["cps-warn"] = 22,

    -- Amount of seconds to subtract from the current timestamp when pressing shift+x / shift+c
    offset = 0.2,
    -- Amount of seconds to seek with F3 / F5
    seek = 1,
    -- Amount of seconds to peek with F1 / F10
    peek = 1,
    -- Amount of seconds to peek with shift+F1 / shift+F10
    ["peek-more"] = 2.5
}

mp.options = require "mp.options"
mp.options.read_options(options, "subber")

mp.input = require "mp.input"



local error_style = "{\\c&H7a77f2&}"
local error_terminal_style = "\027[31m"


--- timestamp formatted as expected in various file formats
local formatted_timestamp = {
    webvtt = function(seconds) a_number(seconds)
        return string.format("%02d:%02d:%05.2f", seconds/(60*60), seconds/60%60, seconds%60)
    end,

    ass = function(seconds) a_number(seconds)
        return string.format("%d:%02d:%05.2f", seconds/(60*60), seconds/60%60, seconds%60)
    end
}
formatted_timestamp.sub = formatted_timestamp.webvtt

local formatted_sub_line = {
    webvtt = function(start_time, end_time, text) a_number(start_time, end_time) a_string(text)
        return a_string(
            "\n" ..
            formatted_timestamp.webvtt(start_time) .. " --> " ..
            formatted_timestamp.webvtt(end_time) .. "\n" ..
            text .. "\n"
        )
    end,

    ass = function(start_time, end_time, text) a_number(start_time, end_time) a_string(text)
        local formatted_text = text:gsub("\n", "\\N")

        return a_string(
            -- Category: Layer
            "Dialogue: 0," ..
            -- Start, End
            formatted_timestamp.ass(start_time) .. "," ..
            formatted_timestamp.ass(end_time) .. "," ..
            -- Style, Name, MarginL, MarginR, MarginV, Effect
            "Default,,0,0,0,," ..
            -- Text
            formatted_text .. "\n"
        )
    end,

    sub = function(start_time, end_time, text) a_number(start_time, end_time) a_string(text)
        local formatted_text = text:gsub("\n", "\\N")

        return a_string(
            formatted_timestamp.sub(start_time) .. "," ..
            formatted_timestamp.sub(end_time) .. "," ..
            formatted_text .. "\n"
        )
    end
}

local function timestamp_formatter(sub_type) a_string(sub_type)
    local format_function = formatted_timestamp[sub_type]
    if format_function == nil then format_function = formatted_timestamp.sub end

    return a_function(format_function)
end

local function formatted_timestamps_for_display(mb_seconds1, mb_seconds2) a_mb_number(mb_seconds1, mb_seconds2)
    local formatted_1 = ""
    local formatted_2 = ""
    if mb_seconds1 ~= nil then formatted_1 = formatted_timestamp.ass(mb_seconds1) end
    if mb_seconds2 ~= nil then formatted_2 = formatted_timestamp.ass(mb_seconds2) end
    return a_string(formatted_1 .. "," .. formatted_2)
end



local function to_system_clipboard(text) a_string(text)
    local pipe = assert(io.popen("wl-copy", "w"))
    --local pipe = assert(io.popen("xclip -i -selection clipboard", "w"))
    pipe:write(text)
    pipe:close()
end

local function update_cps_warning(text, sub_duration) a_string(text) a_number(sub_duration)
    if sub_duration <= 0 then
        mp.input.set_log({
            {
                text = "Empty time region",
                style = error_style,
                terminal_style = error_terminal_style,
            }
        })
        return
    end

    local cps = math.floor(string.len(text) / sub_duration)
    if cps >= options["cps-warn"] then
        mp.input.set_log({
            {
                text = string.format("%d CPS", cps),
                style = error_style,
                terminal_style = error_terminal_style,
            }
        })
    else
        mp.input.set_log({
            string.format("%d CPS", cps)
        })
    end
end

local function seek(timestamp) a_number(timestamp)
    mp.command_native({"seek", timestamp, "absolute+exact"})
end

local entered_text = ""

local function get_user_input(sub_duration, on_submit, mb_on_discard)
        a_number(sub_duration) a_function(on_submit) a_mb_function(mb_on_discard)
    local submitted = false

    mp.input.get({
        prompt = "New subtitle:",
        default_text = entered_text,
        edited = function(text) a_string(text)
            update_cps_warning(text, sub_duration)
        end,
        submit = function(text) a_string(text)
            submitted = true
            entered_text = ""
            mp.input.terminate()
            on_submit(text)
        end,
        closed = function(text) a_string(text)
            if not submitted and mb_on_discard ~= nil then
                local on_discard = mb_on_discard
                entered_text = text
                on_discard(text)
            end
        end
    })
    update_cps_warning(entered_text, sub_duration)
end

local function could_append_to_file(filename, text) a_string(filename, text)
    local success, mb_fd = pcall(function()
        return a_mb_userdata(io.open(filename, "a"))
    end)
    if not success or mb_fd == nil then
        return false
    end
    local fd = a_userdata(mb_fd)

    success = pcall(function()
        fd:write(text)
        fd:flush()
    end)
    pcall(function()
        fd:close()
    end)
    return a_boolean(success)
end



local overlay = mp.create_osd_overlay("ass-events")
local function osd_display_range(time_start, time_end) a_number(time_start, time_end)
    overlay.data = "TS: " .. formatted_timestamps_for_display(time_start, time_end)
    overlay:update()
end

local function get_time_position()
    local time_pos = mp.get_property_native("time-pos")
    assert(time_pos ~= nil, "Time position in file unknown")
    return time_pos
end



local function could_create_ass_file(sub_filename, media_filename) a_string(sub_filename)
    local success = pcall(function()
        local fd = io.open(sub_filename, "w")
        fd:write(string.format("[Script Info]\n\n[Aegisub Project Garbage]\nAudio File: %s\nVideo File: %s\n\n" ..
            "[Events]\nFormat: Layer, Start, End, Style, Name, MarginL, MarginR, MarginV, Effect, Text\n",
            media_filename, media_filename))
        fd:flush()
        fd:close()
    end)
    return success
end


local function add_subtitle(start_time, end_time, text)
        a_number(start_time, end_time) a_string(text)

    local mb_sub_type, mb_filename = (function()
        local mb_active_sub_type = mp.get_property_native("current-tracks/sub/codec")
        local mb_active_sub_filename = mp.get_property_native("current-tracks/sub/external-filename")

        if mb_active_sub_filename ~= nil then
            return mb_active_sub_type, mb_active_sub_filename
        end

        local path = mp.get_property_native("path")
        if path == nil then return nil, nil end

        return "ass", (path:gsub("%.[^%./]+$", "") .. ".ass")
    end)()

    local track_type_recognized, format_function = (function()
        local mb_format_function = formatted_sub_line[mb_sub_type]
        if mb_format_function == nil then
            return false, formatted_sub_line.sub
        else
            return true, mb_format_function
        end
    end)()

    local line = format_function(start_time, end_time, text)

    local saved_to_file, file_created = (function()
        -- If the file does not exist yet, create the ASS headers
        local file_created
        if mb_filename ~= nil then
            local filename = mb_filename

            local file_openable_for_reading = (function()
                local fd = io.open(filename, "r")
                if fd ~= nil then
                    fd:close()
                    return true
                else
                    return false
                end
            end)()

            if not file_openable_for_reading then
                -- Caveat: if an ASS subtitle file exists that is not readable but is writable, it
                -- will be overwritten. You can blame the lack of a "file exists" function in Lua's
                -- standard library for this.
                assert(mb_sub_type == "ass")
                local success = could_create_ass_file(filename, mp.get_property_native("path"))
                if not success then
                    return false, false
                end
                file_created = true
            else
                file_created = false
            end
        else
            file_created = false
        end

        if not track_type_recognized or mb_filename == nil then
            return false, a_boolean(file_created)
        else
            local filename = mb_filename
            return could_append_to_file(filename, line), a_boolean(file_created)
        end
    end)()

    if saved_to_file then
        assert(mb_filename ~= nil)
        local filename = mb_filename

        if file_created then
            mp.commandv("sub-add", filename)
            mp.osd_message("\n\nSaved to new sub file " .. filename, 2)
        else
            mp.osd_message("\n\nSaved to sub", 2)
        end
        mp.command("sub-reload")
    else
        to_system_clipboard(line)

        local reason = (function()
            if mb_sub_type == nil then
                return "not written to file because mpv did not report a sub type"
            elseif not track_type_recognized then
                return "not written to file because of unknown sub type " .. mb_sub_type
            elseif mb_filename == nil then
                return "no external sub file"
            else
                return "failed to write to file"
            end
        end)()
        mp.osd_message("\n\nCopied to clipboard (" .. reason .. ")", 4)
    end
end

local function copy_timestamp()
    local mb_sub_type = mp.get_property_native("current-tracks/sub/codec")
    local time_pos = get_time_position()
    local timestamp = timestamp_formatter(mb_sub_type)(time_pos)
    to_system_clipboard(timestamp)
    mp.osd_message("\n\nCopied " .. timestamp, 2)
end


local mb_timeregion_start = nil
local mb_timeregion_end = nil

local mb_timepos_observer = nil

local function cancel_pause_at()
    if mb_timepos_observer ~= nil then
        local timepos_observer = mb_timepos_observer
        mp.unobserve_property(timepos_observer)
    end
end

local function pause_at(timestamp, mb_callback) a_number(timestamp) a_mb_function(mb_callback)
    cancel_pause_at()
    local function timepos_observer(_name, current_timestamp)
        if current_timestamp >= timestamp then
            mp.set_property_native("pause", true)
            mp.unobserve_property(timepos_observer)
            if mb_callback ~= nil then
                local callback = mb_callback
                callback()
            end
        end
    end
    mb_timepos_observer = timepos_observer
    mp.observe_property("time-pos", "number", timepos_observer)
end

local timeregion_playing = false

local function start_timeregion(mb_offset, mb_time_pos_instead_of_current)
        a_mb_number(mb_offset, mb_time_pos_instead_of_current)
    local offset = mb_offset or 0

    local time_pos = mb_time_pos_instead_of_current or get_time_position()

    -- The previous frame is taken, so that when you start a sub on one frame, the sub is
    -- visible on that frame instead of the next
    local frame_correcting_offset = (function()
        if mb_time_pos_instead_of_current ~= nil then
            return 0
        end

        if time_pos == 0 then
            return 0
        end

        local frame_rate = mp.get_property_native("current-tracks/video/demux-fps") or 100
        local frame_duration = 1 / frame_rate
        return -frame_duration
    end)()

    cancel_pause_at()
    timeregion_playing = false
    mb_timeregion_start = time_pos + frame_correcting_offset + offset
    osd_display_range(mb_timeregion_start, nil)
end

local function finish_timeregion(mb_offset) a_mb_number(mb_offset)
    local offset = mb_offset or 0

    if mb_timeregion_start == nil then
        mp.osd_message("No sub line start selected")
        return
    end
    local timeregion_start = a_number(mb_timeregion_start)

    mp.set_property_native("pause", true)
    timeregion_playing = false
    local timeregion_end = get_time_position() + offset
    mb_timeregion_end = timeregion_end

    osd_display_range(timeregion_start, timeregion_end)

    local on_discard = function(text) a_string(text)
        cancel_pause_at()
        timeregion_playing = false
        osd_display_range(timeregion_start, nil)
    end

    get_user_input(
        timeregion_end - timeregion_start,
        -- on_submit
        function(sub_text) a_string(sub_text)
            if sub_text == "" then
                on_discard("")
            else
                add_subtitle(timeregion_start, timeregion_end, sub_text)
                seek(timeregion_end)
                start_timeregion(nil, timeregion_end)
                mb_timeregion_end = nil
            end
        end,
        on_discard
    )
end

local function timeregion_play_from_start()
    if mb_timeregion_start == nil then
        mp.osd_message("No active time region", 2)
        return
    end
    local timeregion_start = mb_timeregion_start

    seek(timeregion_start)

    if mb_timeregion_end ~= nil then
        local timeregion_end = mb_timeregion_end
        timeregion_playing = true
        pause_at(timeregion_end, function() timeregion_playing = false end)
    else
        timeregion_playing = false
    end

    mp.set_property_native("pause", false)
end

local function timeregion_play_pause()
    if timeregion_playing ~= true then
        timeregion_play_from_start()
    else
        mp.command("cycle pause")
    end
end

local function seek_timeregion(seconds) a_number(seconds)
    local current_timestamp = get_time_position()
    local new_timestamp = current_timestamp + seconds
    timeregion_playing = true
    if mb_timeregion_start ~= nil and new_timestamp < mb_timeregion_start then
        mp.osd_message("\nStart of time region", 0.5)
        new_timestamp = mb_timeregion_start
    elseif mb_timeregion_end ~= nil and new_timestamp > mb_timeregion_end then
        mp.osd_message("\nEnd of time region", 0.5)
        new_timestamp = mb_timeregion_end
        timeregion_playing = false
    end
    seek(new_timestamp)
end

local function peek_before_timeregion(peek_seconds) a_number(peek_seconds)
    if mb_timeregion_start == nil then
        start_timeregion()
    end

    local timeregion_start = mb_timeregion_start
    seek(timeregion_start - peek_seconds)
    pause_at(
        timeregion_start,
        function()
            timeregion_playing = false
            -- The time region start may have changed by the user during playback
            if mb_timeregion_start ~= nil then
                timeregion_start = mb_timeregion_start
                seek(timeregion_start)
            end
        end
    )
    mp.set_property_native("pause", false)
end

local function peek_after_timeregion(peek_seconds) a_number(peek_seconds)
    if mb_timeregion_end == nil then
        if mb_timeregion_start ~= nil and get_time_position() < mb_timeregion_start + 0.2 then
            -- If pressing F10 near the start of the time region, no can do
            mp.osd_message("\nNo end of time region to peek beyond", 2)
        else
            finish_timeregion()
        end
    end
    if mb_timeregion_end ~= nil then
        local timeregion_end = mb_timeregion_end
        seek(timeregion_end)
        pause_at(
            timeregion_end + peek_seconds,
            function()
                timeregion_playing = false
                -- The time region end may have changed by the user during playback
                if mb_timeregion_end ~= nil then
                    timeregion_end = mb_timeregion_end
                    seek(timeregion_end)
                end
            end
        )
        mp.set_property_native("pause", false)
    end
end


mp.add_key_binding("x", "start-time-region", start_timeregion)
mp.add_key_binding("X", "start-time-region-offset", function() start_timeregion(-options.offset) end)
mp.add_key_binding("c", "finish-time-region", finish_timeregion)
mp.add_key_binding("C", "finish-time-region-offset", function() finish_timeregion(-options.offset) end)

--mp.add_key_binding("ctrl+space", "play-pause-time-region", play_pause_timeregion)
mp.add_key_binding("F1", "peek-before-time-region", function()
    peek_before_timeregion(options.peek) end)
mp.add_key_binding("shift+F1", "peek-more-before-time-region", function()
    peek_before_timeregion(options["peek-more"]) end)
mp.add_key_binding("F3", "time-region-seek-backwards", function()
    seek_timeregion(-options.seek) end)
mp.add_key_binding("shift+F3", "time-region-seek-start", function()
    if mb_timeregion_start ~= nil then seek(mb_timeregion_start) end end)
mp.add_key_binding("F4", "play-pause-time-region", timeregion_play_pause)
mp.add_key_binding("shift+F4", "play-time-region-from-start", timeregion_play_from_start)
mp.add_key_binding("F5", "time-region-seek-forwards", function()
    seek_timeregion(options.seek) end)
mp.add_key_binding("shift+F5", "time-region-seek-end", function()
    if mb_timeregion_end ~= nil then seek(mb_timeregion_end) end end)
mp.add_key_binding("F10", "peek-after-time-region", function()
    peek_after_timeregion(options.peek) end)
mp.add_key_binding("shift+F10", "peek-more-after-time-region", function()
    peek_after_timeregion(options["peek-more"]) end)

mp.add_key_binding("ctrl+c", "copy-timestamp", copy_timestamp)
mp.add_key_binding("ctrl+r", "sub-reload", function()
    mp.command("sub-reload")
    mp.osd_message("\n\nSubs reloaded", 0.5)
end)
